#include "locator.h"
int app1_main( void ) {
    init();
    app_event_loop();
    deinit();
  return 0;
}
                            
void init(void) {
  select = false;
  tracking = false;
  angle = -1000;
  dist = -1000;
  
  image = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_ARROW);
  bitmap_layer_set_alignment((BitmapLayer*)image , GAlignLeft);
  
  window = window_create();
  text_layer = text_layer_create(GRect(25, 120, 30, 30));
  
  window_set_click_config_provider(window, click_config_provider);
  app_comm_set_sniff_interval(SNIFF_INTERVAL_REDUCED); 
  // Register AppMessage handlers
  app_message_register_inbox_received(in_received_handler); 
  
  //open the message inbox and outbox
  app_message_open(app_message_inbox_size_maximum(), app_message_outbox_size_maximum());
  
  Layer *window_layer = window_get_root_layer(window);
  rot = rot_bitmap_layer_create(image);
  layer_add_child(window_layer, (Layer *) rot);
  window_stack_push(window, true);
  bitmap_layer_set_alignment((BitmapLayer*)rot , GAlignCenter);

  //subscribe to compass updates
  compass_service_subscribe(compass_handler);
  compass_service_set_heading_filter (1*TRIG_MAX_ANGLE/360);		
  //app_event_loop();
    //deinit();  
   display_help_splash();
  layer_add_child((Layer *)rot, text_layer_get_layer(text_layer));

}

void display_help_splash(){
  

 TextLayer* help_layer = text_layer_create(GRect(0, 0, 144, 154));
	
	//Set the text, font, and text alignment
	text_layer_set_text(help_layer, "Press select to mark your current location");
	text_layer_set_font(help_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD));
	text_layer_set_text_alignment(help_layer, GTextAlignmentCenter);
  

}
// Called when a message is received from PebbleKitJS
static void in_received_handler(DictionaryIterator *received, void *context) {
  
    int command;
  
    Tuple *tuple;

    tuple = dict_find(received, ANGLE_KEY);
    if(tuple) {
        //APP_LOG(APP_LOG_LEVEL_DEBUG, "Received Angle: %d", tuple->value->int32);
        angle = tuple->value->int16;
        if(angle < 0){
          angle += 360;
        }
        
    }

    tuple = dict_find(received, DIST_KEY);
    if(tuple) {
       //APP_LOG(APP_LOG_LEVEL_DEBUG, "Received Distance: %d", tuple->value->int32);
        dist = tuple->value->int32;
    }
    //checks the current compass orientation
}



static void select_click_handler(ClickRecognizerRef recognizer, void *context) {
  select = true;
  send_message(1);
}

static void up_click_handler(ClickRecognizerRef recognizer, void *context) {
  send_message(2);  
}

static void down_click_handler(ClickRecognizerRef recognizer, void *context) {
  send_message(3);
}

static void click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
  window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
  window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
}

static void compass_handler(CompassHeadingData heading){
  if(angle != -1000){
    int north = heading.magnetic_heading;
  //APP_LOG(APP_LOG_LEVEL_DEBUG, "north:  %d", north);  
  showCompass(angle, dist, heading.true_heading);
  }
  
  
}

// Write message to buffer & send
void send_message(int command)
{
    DictionaryIterator *iter;
    
    app_message_outbox_begin(&iter);
  
    dict_write_uint8(iter, CMD_KEY, command);

    dict_write_end(iter);
    app_message_outbox_send();
}


void showCompass(int angle, int dist, CompassHeading north)
{
  dist *= 90347;
  char* $dist = itoa(dist);
  char $$dist[5];
  for(int i = 0 ; i < 5; i ++){
    $$dist[i] = $dist[i]; 
  }
  text_layer_set_text(text_layer, strcat($$dist, "m"));
  int temp = ((angle * TRIG_MAX_ANGLE) / 360);
  temp  = north - temp;
  if(temp < 0){
    temp += TRIG_MAX_ANGLE;
  }
  rot_bitmap_layer_set_angle(rot, temp);
}


// Implementation of itoa()
char *itoa(int num)
{
static char buff[20] = {};
int i = 0, temp_num = num, length = 0;
char *string = buff;
if(num >= 0) {
// count how many characters in the number
while(temp_num) {
temp_num /= 10;
length++;
}
// assign the number to the buffer starting at the end of the 
// number and going to the begining since we are doing the
// integer to character conversion on the last number in the
// sequence
for(i = 0; i < length; i++) {
 	buff[(length-1)-i] = '0' + (num % 10);
num /= 10;
}
buff[i] = '\0'; // can't forget the null byte to properly end our string
}
else
return "Unsupported Number";
return string;
}

void deinit(void) {
    gbitmap_destroy(image);
    rot_bitmap_layer_destroy(rot);
    app_message_deregister_callbacks();
    window_destroy(window);
    compass_service_unsubscribe();
}




 

	

