#include <pebble.h>
//globabl variables
  
static Window *menu_window;
static MenuLayer *menu_layer;

 
static Window *window;    
static bool tracking;

static GBitmap *image;
static RotBitmapLayer *rot;
TextLayer *text_layer;

bool select;
int angle;
int dist;

// Key values for AppMessage Dictionary
enum {
    ANGLE_KEY = 1,    //ANGLE TO ORIGIN
    DIST_KEY = 2,     //DISTANCE TO ORIGIN
    CMD_KEY = 3
};

//prototypes
int app1_main(void);
void display_help_splash();
void showCompass(int, int, CompassHeading);
void send_message(int command);
char *itoa(int num);
void init(void);
void init_menu(void);
void deinit(void);
void deinit_menu();

//void app_event_loop();

//handler prototypes

static void select_click_handler(ClickRecognizerRef recognizer, void *context);
static void up_click_handler(ClickRecognizerRef recognizer, void *context);
static void down_click_handler(ClickRecognizerRef recognizer, void *context);
static void click_config_provider(void*);
static void compass_handler(CompassHeadingData heading);
static void in_received_handler(DictionaryIterator *received, void *context);
